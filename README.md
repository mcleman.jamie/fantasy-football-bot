# Fantasy Football Bot

# To Do:
- Editting fixtures
- Wildcards and chips
- Double gameweeks
- Look into more detail how different weeks are weighted
- Improve transfers/initial team selection to consider who is actually getting played so dont waste money

# Issues
- How to represent double/triple gameweeks in data
- Injuries (How does it know how long a player is out for?) - for example, a 1 game suspension!
- Change 'week' to 'gameweek' (used in fixtures and individual teams)

# Structure
1. UPDATE PLAYER DATA (new players added, change of teams, price, chanceOfPlaying)
2. UPDATE MATCH EVENTS OF PREVIOUS GAMEWEEK (players and teams)
3. UPDATE FIXTURES
4. MAKE CLEAN SHEET PREDICTIONS
5. MAKE PLAYER PREDICTIONS
6. TRANSFERS
7. TEAM SELECTION