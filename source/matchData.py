import pandas as pd
from understat import Understat
import requests
import asyncio
import aiohttp
import util

#matchEvents = []
homeAwayString = ["h", "a"]

class MatchEvent:
    def __init__(self, HomeID, AwayID, UnderstatID):
        self.homeID = HomeID
        self.awayID = AwayID
        self.understatID = UnderstatID



def getMatchIDsForGW(Gameweek):

    fixtures = pd.read_csv(util.DATA_PATH + "fixtures.csv")
    teams = pd.read_csv(util.DATA_PATH + "teams/teams.csv")
    matchEvents = []
    for index, row in fixtures.iterrows():
        if row["week"] == Gameweek:
            homeTeamName = teams.loc[teams.fantasy_id == row["home"], "name"].values[0]
            awayTeamName = teams.loc[teams.fantasy_id == row["away"], "name"].values[0]

            print("------------------------------")
            print("Enter the understat ID for... ")
            print(homeTeamName + " vs " + awayTeamName)
            while True:
                inputtedID = input()
                try:
                    inputtedID = int(inputtedID)
                    matchEvents.append(MatchEvent(row["home"], row["away"], inputtedID))
                    break
                except:
                    print("Please enter an integer...")

    return matchEvents



async def updateDataFromPastGameweek():

    fantasyData = pd.read_csv(util.DATA_PATH + "fantasy/fantasy.csv")
    pastGameweek = int(fantasyData.loc[fantasyData.index == 0, "upcoming_gameweek"].values[0]) - 1

    matchEvents = getMatchIDsForGW(pastGameweek)

    playersData = pd.read_csv(util.DATA_PATH + "players/players.csv")

    url = 'https://fantasy.premierleague.com/api/bootstrap-static/'
    r = requests.get(url)
    apiPlayers = r.json()["elements"]

    for matchEvent in matchEvents:

        homeGoals = 0
        awayGoals = 0
        homeXG = 0.0
        awayXG = 0.0

        async with aiohttp.ClientSession() as session:

            # Get data from the match
            understat = Understat(session)
            results = await understat.get_match_players(matchEvent.understatID)

            # Get data about the players who played
            for homeAway in homeAwayString:
                for key in results[homeAway]:

                    # Get understat ID and fantasy ID
                    understatID = results[homeAway][key]["player_id"]
                    #print(understatID)
                    fantasyID = playersData.loc[playersData.understat_id == int(understatID), "fantasy_id"]

                    # If found
                    if len(fantasyID.values) > 0:

                        fantasyID = fantasyID.values[0]
                        index = playersData.loc[playersData.fantasy_id == int(fantasyID), "index"].values[0]
                        position = playersData.loc[playersData.fantasy_id == int(fantasyID), "position"].values[0]

                        path = util.DATA_PATH + "players/players/" + str(fantasyID) + ".csv"
                        individualPlayerData = pd.read_csv(path).set_index("gameweek")

                        # Get the data for this player from this game
                        minutes = float(results[homeAway][key]["time"])
                        goals = float(results[homeAway][key]["goals"])
                        bonus = apiPlayers[index]["bonus"] - individualPlayerData.bonus["total"]
                        points = apiPlayers[index]["total_points"] - individualPlayerData.points["total"]
                        price = apiPlayers[index]["now_cost"] / 10.0
                        ownGoals = int(results[homeAway][key]["own_goals"])
                        xG = 0

                        # Goalkeepers
                        if position == 1:
                            saves = apiPlayers[index]["saves"] - individualPlayerData.saves["total"]
                            individualPlayerData.loc[individualPlayerData.index == str(pastGameweek)] = [minutes, saves, bonus, points, price, 1]
                        # Outfield players
                        else:
                            xG = float(results[homeAway][key]["xG"])
                            assists = float(results[homeAway][key]["assists"])
                            xA = float(results[homeAway][key]["xA"])

                            individualPlayerData.loc[individualPlayerData.index == str(pastGameweek)] = [minutes, goals, assists, xG, xA, bonus, points, price, 1]

                        # Update the score of the game
                        if homeAway == "h":
                            homeGoals += int(goals)
                            awayGoals += ownGoals
                            homeXG += float(xG)
                        else:
                            awayGoals += int(goals)
                            homeGoals += ownGoals
                            awayXG += float(xG)

                        # Update total
                        individualPlayerData.loc[individualPlayerData.index == "total"] = individualPlayerData.loc[[str(pastGameweek), "total"], :].sum().values
                        individualPlayerData.available["total"] = 0

                        # Save the total
                        print(str(results[homeAway][key]["player"]) + ": " + str(minutes))
                        print(individualPlayerData)
                        individualPlayerData.to_csv(path)


                    else:
                        print("Not found: " + results[homeAway][key]["player"])
                        xG = float(results[homeAway][key]["xG"])
                        goals = float(results[homeAway][key]["goals"])
                        ownGoals = int(results[homeAway][key]["own_goals"])

                        # Update the score of the game
                        if homeAway == "h":
                            homeGoals += int(goals)
                            awayGoals += ownGoals
                            homeXG += float(xG)
                        else:
                            awayGoals += int(goals)
                            homeGoals += ownGoals
                            awayXG += float(xG)

            # Update data about players who didnt play
            # Iterate through all players
            for index, row in playersData.iterrows():

                # If the team id matches the home team id or away team id
                if row["team_id"] == matchEvent.homeID or row["team_id"] == matchEvent.awayID:

                    # If there isnt anything in the current gameweek
                    path = util.DATA_PATH + "players/players/" + str(row["fantasy_id"]) + ".csv"
                    individualPlayerData = pd.read_csv(path).set_index("gameweek")

                    #print(individualPlayerData.loc[individualPlayerData.index == str(pastGameweek), "minutes"].any())

                    print(row["web_name"])

                    if individualPlayerData.loc[individualPlayerData.index == str(pastGameweek), "minutes"].any() == False:

                        print("DIDNT PLAY")

                        position = row["position"]

                        # Check their availability
                        available = 1
                        if row["chance_of_playing"] < 100:
                            available = 0

                        # Fill with 0's
                        if position == 1:
                            individualPlayerData.loc[individualPlayerData.index == str(pastGameweek)] = [0, 0, 0, 0, row["price"], available]
                        else:
                            individualPlayerData.loc[individualPlayerData.index == str(pastGameweek)] = [0, 0, 0, 0, 0, 0, 0, row["price"], available]

                        # Save
                        #print(individualPlayerData)
                        individualPlayerData.to_csv(path)

            # Update team data
            homeTeamPath = util.DATA_PATH + "teams/" + str(matchEvent.homeID) + ".csv"
            homeTeam = pd.read_csv(homeTeamPath).set_index("week") # CHANGE TO 'gameweek'
            homeTeam.loc[homeTeam.index == str(pastGameweek)] = [homeGoals, awayGoals, homeXG, awayXG, "H"]

            awayTeamPath = util.DATA_PATH + "teams/" + str(matchEvent.awayID) + ".csv"
            awayTeam = pd.read_csv(awayTeamPath).set_index("week") # CHANGE TO 'gameweek'
            awayTeam.loc[awayTeam.index == str(pastGameweek)] = [awayGoals, homeGoals, awayXG, homeXG, "A"]

            print(homeTeam)
            print(awayTeam)

            # Save
            homeTeam.to_csv(homeTeamPath)
            awayTeam.to_csv(awayTeamPath)



if __name__ == "__main__":

    loop = asyncio.get_event_loop()
    loop.run_until_complete(updateDataFromPastGameweek())
