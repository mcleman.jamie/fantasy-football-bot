import pandas as pd
import math
import util

def makeTransfers():
    currentTeam = pd.read_csv(util.DATA_PATH + "fantasy/team.csv").set_index("index")
    fantasyData = pd.read_csv(util.DATA_PATH + "fantasy/fantasy.csv").set_index("index")
    playersData = pd.read_csv(util.DATA_PATH + "players/players.csv")

    print(currentTeam)
    print(fantasyData)

    numFreeTransfers = fantasyData.loc[fantasyData.index == 0, "free_transfers"].values[0]
    moneyITB = fantasyData.loc[fantasyData.index == 0, "money_itb"].values[0]

    print("Free transfers: " + str(numFreeTransfers))
    print("Money ITB:  " + str(moneyITB))

    transferData = pd.DataFrame([], columns=["out_id", "in_id", "delta_points", "delta_money_itb"])
    transfer = pd.Series(data=[-1.0, -1.0, -1.0, -1.0], index=["out_id", "in_id", "delta_points", "delta_money_itb"])


    # Iterate through every player in the current team
    for i in range(0, int(numFreeTransfers)):
        for index, row in currentTeam.iterrows():

            fantasyID = row["fantasy_id"]
            currentPredictedPoints = playersData.loc[playersData.fantasy_id == fantasyID, "predicted_points_next_5"].values[0]
            currentPredictedPointsNextGame = playersData.loc[playersData.fantasy_id == fantasyID, "predicted_points_next_game"].values[0]
            currentName = playersData.loc[playersData.fantasy_id == fantasyID, "web_name"].values[0]
            print("Selling: " + currentName)

            # Calculate the selling price
            purchasePrice = row["purchase_price"]
            currentPrice = playersData.loc[playersData.fantasy_id == fantasyID, "price"].values[0]
            sellingPrice = currentPrice
            if currentPrice > purchasePrice:
                sellingPrice = math.floor((purchasePrice + ((currentPrice - purchasePrice) / 2.0)) * 10.0) / 10.0

            # Find the best replacement for this player
            # NEED TO CONSIDER THE MAX PLAYERS FROM A SINGLE TEAM CONSTRAINT
            position = playersData.loc[playersData.fantasy_id == fantasyID, "position"].values[0]
            budget = sellingPrice + moneyITB
            suitablePlayers = playersData.loc[playersData.position == position]
            suitablePlayers = suitablePlayers.loc[suitablePlayers.price <= budget]
            suitablePlayers = suitablePlayers.sort_values(by="predicted_points_next_5", ascending=False).reset_index()

            if len(suitablePlayers.values) > 0:
                replacementPlayerIndex = 0
                while True:
                    replacementPlayerID = suitablePlayers.loc[suitablePlayers.index == replacementPlayerIndex, "fantasy_id"].values[0]
                    # Check if we already have this player
                    if len(currentTeam.loc[currentTeam.fantasy_id == replacementPlayerID].values) > 0:
                        replacementPlayerIndex += 1
                    else:
                        replacementName = suitablePlayers.loc[suitablePlayers.index == replacementPlayerIndex, "web_name"].values[0]
                        replacementPrice = suitablePlayers.loc[suitablePlayers.index == replacementPlayerIndex, "price"].values[0]
                        print("Replacing with: " + replacementName)
                        break

            replacementPredictedPoints = playersData.loc[playersData.fantasy_id == replacementPlayerID, "predicted_points_next_5"].values[0]
            replacementPredictedPointsNextGame = playersData.loc[playersData.fantasy_id == replacementPlayerID, "predicted_points_next_game"].values[0]

            deltaPointsNext5 = replacementPredictedPoints - currentPredictedPoints
            deltaPointsNextGame = replacementPredictedPointsNextGame - currentPredictedPointsNextGame
            print("Change in points next 5: " + str(deltaPointsNext5))
            print("Change in points next game: " + str(deltaPointsNextGame))
            print("----------------------------------")

            # If the replacement points next game are worse than current player,
            if deltaPointsNextGame < 0:
                # Ignore this possible transfer
                continue
            if deltaPointsNext5 > transfer[2]:
                transfer["in_id"] = replacementPlayerID
                transfer["out_id"] = fantasyID
                transfer["delta_points"] = deltaPointsNext5
                transfer["delta_money_itb"] = sellingPrice - replacementPrice

        #print(transfer)

        currentTeam.loc[currentTeam.fantasy_id == transfer["out_id"], "fantasy_id"] = transfer["in_id"]
        currentTeam.loc[currentTeam.fantasy_id == transfer["in_id"], "purchase_price"] = playersData.loc[playersData.fantasy_id == transfer["in_id"], "price"].values[0]
        moneyITB += transfer["delta_money_itb"]
        numFreeTransfers -= 1

        #print(currentTeam)

        transferData = transferData.append(transfer, ignore_index=True)
        #print(transferData)
        transfer["in_id"] = -1.0
        transfer["out_id"] = -1.0
        transfer["delta_points"] = -1.0
        transfer["delta_money_itb"] = -1.0


    # Update fantasy data
    fantasyData.loc[fantasyData.index == 0, "money_itb"] = round(moneyITB, 1)
    fantasyData.loc[fantasyData.index == 0, "free_transfers"] = int(numFreeTransfers)
    print(fantasyData)
    print(currentTeam)

    # Output changes needing to be made
    for index, row in transferData.iterrows():
        sellingName = playersData.loc[playersData.fantasy_id == row["out_id"], "web_name"].values[0]
        buyingName = playersData.loc[playersData.fantasy_id == row["in_id"], "web_name"].values[0]
        print("Sell " + sellingName + " for " + buyingName + "!")


    # Save CSV files
    currentTeam.to_csv(util.DATA_PATH + "fantasy/team.csv")
    fantasyData.to_csv(util.DATA_PATH + "fantasy/fantasy.csv")



if __name__ == "__main__":
    makeTransfers()
