import pandas as pd
import numpy as np
import teamsData
import util

# TO DO:
# - Incorporate data from current season
# - Add cleanSheetPercentage and failedToScorePercentage
# - Add home advantage

def cleanSheetProbability():

    # Get upcoming gameweek
    fantasyData = pd.read_csv(util.DATA_PATH + "fantasy/fantasy.csv")
    upcomingGameweek = int(fantasyData.loc[fantasyData.index == 0, "upcoming_gameweek"].values[0])

    # Load team data
    teams = pd.read_csv(util.DATA_PATH + "teams/teams.csv").set_index("fantasy_id")

    # Load fixture data
    fixtures = pd.read_csv(util.DATA_PATH + "fixtures.csv")

    previousCSData = pd.read_csv(util.DATA_PATH + "teams/teamCleanSheetData.csv")

    # Load in formula for converting between xGA and chance of clean sheet
    xGACleanSheetProbabilityFormula = pd.read_csv(util.DATA_PATH + "xGACleanSheetProbabilityFormula.csv")
    xGAFormula = np.poly1d(xGACleanSheetProbabilityFormula.coefficient)

    # Create list of next 5 gameweeks
    gameweekList = []
    for i in range(0, 5):
        gameweekList.append(upcomingGameweek + i)

    # Create empty dataframe
    cleanSheetData = pd.DataFrame([], index=teams.index, columns = gameweekList)

    # Iterate through all fixtures
    for gameweek in gameweekList:
        for index, row in fixtures.iterrows():
            if (row["week"] == gameweek):

                # Get data
                homeID = row["home"]
                homeData = pd.read_csv(util.DATA_PATH + "teams/" + str(homeID) + ".csv").set_index("week")
                awayID = row["away"]
                awayData = pd.read_csv(util.DATA_PATH + "teams/" + str(awayID) + ".csv").set_index("week")

                # Find xG and xGA data
                homeXG = 0
                homeXGA = 0
                awayXG = 0
                awayXGA = 0
                for i in range(1, upcomingGameweek):
                    homeXG += homeData.xG[str(i)]
                    homeXGA += homeData.xGA[str(i)]
                    awayXG += awayData.xG[str(i)]
                    awayXGA += awayData.xGA[str(i)]

                homeXG += homeData.xG["2020/21"]
                homeXGA += homeData.xGA["2020/21"]
                awayXG += awayData.xG["2020/21"]
                awayXGA += awayData.xGA["2020/21"]

                homeXG = homeXG / upcomingGameweek
                homeXGA = homeXGA / upcomingGameweek
                awayXG = awayXG / upcomingGameweek
                awayXGA = awayXGA / upcomingGameweek

                # Get historical clean sheet percentages and failed to score percentages
                homeCSPercentage = previousCSData.loc[previousCSData.fantasy_id == homeID, "home_cs"].values[0]
                awayCSPercentage = previousCSData.loc[previousCSData.fantasy_id == awayID, "away_cs"].values[0]
                homeFailedToScore = previousCSData.loc[previousCSData.fantasy_id == homeID, "home_failed_to_score"].values[0]
                awayFailedToScore = previousCSData.loc[previousCSData.fantasy_id == awayID, "away_failed_to_score"].values[0]

                # Calculate historical clean sheet percentages from this data
                historicalHomeCSPercentage = (homeCSPercentage + awayFailedToScore) / 2.0
                historicalAwayCSPercentage = (awayCSPercentage + homeFailedToScore) / 2.0

                # Predict xGA for both teams
                predictedHomeXGA = (homeXGA + awayXG) / 2.0
                predictedAwayXGA = (awayXGA + homeXG) / 2.0

                # Calculate probability based on xGA
                predictedXGHomeCSPercentage = xGAFormula(predictedHomeXGA)
                predictedXGAwayCSPercentage = xGAFormula(predictedAwayXGA)

                # Set predictions in the dataframe
                cleanSheetData[gameweek][homeID] = (predictedXGHomeCSPercentage + historicalHomeCSPercentage) / 2.0
                cleanSheetData[gameweek][awayID] = (predictedXGAwayCSPercentage + historicalAwayCSPercentage) / 2.0

                # Output
                print(teamsData.getNameFromFantasyID(homeID) + " vs " + teamsData.getNameFromFantasyID(awayID))
                print(str(cleanSheetData[gameweek][homeID]) + " : " + str(cleanSheetData[gameweek][awayID]))

    # Add mean column
    cleanSheetData['mean'] = cleanSheetData.mean(1)

    # Output and save
    cleanSheetData.to_csv(util.DATA_PATH + "teams/cleanSheetsPredicted.csv")


if __name__ == "__main__":
    cleanSheetProbability()
