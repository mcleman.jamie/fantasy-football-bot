import pandas as pd
import util

# Minimum required players for each position
MINIMUM_POSITION = {1 : 1, 2 : 3, 3 : 2, 4 : 1}

# STARTING LINEUP CODE:
LINEUP_CODE = {"Undefined" : -1, "Starting" : 0, "Captain" : 1, "Vice Captain" : 2, "Bench" : 3}

def selectStartingLineup():

    # Load data
    team = pd.read_csv(util.DATA_PATH + "fantasy/team.csv").set_index("index")
    playersData = pd.read_csv(util.DATA_PATH + "players/players.csv").set_index("index")

    projectedPoints = []
    position = []
    lineupCode = []

    # Get projected points and positions of the team and add data to team dataframe
    for index, row in team.iterrows():
        id = row["fantasy_id"]

        projectedPoints.append(playersData.loc[playersData.fantasy_id == id, "predicted_points_next_game"].values[0])
        position.append(playersData.loc[playersData.fantasy_id == id, "position"].values[0])
        lineupCode.append(LINEUP_CODE["Undefined"])

    team["position"] = position
    team["projected_points"] = projectedPoints
    team["lineup_code"] = lineupCode
    numPlayersStarting = 0


    # Get best players in each position (minimum required for each position)
    for pos in range(1, 5):
        playersInPosition = team[team["position"] == pos].sort_values(by="projected_points", ascending=False)

        minimumNumber = MINIMUM_POSITION[pos]
        count = 0
        for index, row in playersInPosition.iterrows():
            team.loc[team.index == index, "lineup_code"] = LINEUP_CODE["Starting"]
            count += 1
            numPlayersStarting += 1
            if count == minimumNumber:
                break

    # Choose the remaining best players
    remainingPlayers = team[team["lineup_code"] == LINEUP_CODE["Undefined"]]
    remainingPlayers = remainingPlayers[remainingPlayers["position"] > 1]
    remainingPlayers = remainingPlayers.sort_values(by="projected_points", ascending=False)
    for index, row in remainingPlayers.iterrows():
        team.loc[team.index == index, "lineup_code"] = LINEUP_CODE["Starting"]
        numPlayersStarting += 1
        if numPlayersStarting == 11:
            break


    # Choose captain and vice captain
    startingLineup = team[team["lineup_code"] == LINEUP_CODE["Starting"]].sort_values(by="projected_points", ascending=False)
    startingLineup = startingLineup.reset_index()
    captainID = startingLineup[startingLineup.index == 0]["fantasy_id"].values[0]
    viceCaptainID = startingLineup[startingLineup.index == 1]["fantasy_id"].values[0]

    team.loc[team.fantasy_id == captainID, "lineup_code"] = LINEUP_CODE["Captain"]
    team.loc[team.fantasy_id == viceCaptainID, "lineup_code"] = LINEUP_CODE["Vice Captain"]

    # Set bench
    team.loc[team.lineup_code == LINEUP_CODE["Undefined"], "lineup_code"] = LINEUP_CODE["Bench"]


    # Output the starting lineup
    print("\nSTARTING LINEUP")
    for index, row in team.iterrows():
        id = row["fantasy_id"]
        name = playersData.loc[playersData.fantasy_id == id, "web_name"].values[0]
        positionName = util.POSITION_CODE_NAME[row.position]
        if row["lineup_code"] == LINEUP_CODE["Starting"]:
            print(positionName + ": "+ name)
        elif row["lineup_code"] == LINEUP_CODE["Captain"]:
            print(positionName + ": "+ name + " (C)")
        elif row["lineup_code"] == LINEUP_CODE["Vice Captain"]:
            print(positionName + ": "+ name + " (VC)")

    # Output the bench, in order
    print("\nBENCH")
    bench = team[team.lineup_code == LINEUP_CODE["Bench"]].sort_values(by="projected_points", ascending=False)

    for index, row in bench.iterrows():
        if row.position == 1:
            id = row.fantasy_id
            name = playersData.loc[playersData.fantasy_id == id, "web_name"].values[0]
            positionName = util.POSITION_CODE_NAME[row.position]
            print(positionName + ": " + name)
            break

    for index, row in bench.iterrows():
        if row.position != 1:
            id = row.fantasy_id
            name = playersData.loc[playersData.fantasy_id == id, "web_name"].values[0]
            positionName = util.POSITION_CODE_NAME[row.position]
            print(positionName + ": " + name)


if __name__ == "__main__":
    selectStartingLineup()
