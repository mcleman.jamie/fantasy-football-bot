import pandas as pd
import util

def updateFantasyData():

    # Read fantasy data
    fantasyData = pd.read_csv(util.DATA_PATH + "fantasy/fantasy.csv").set_index("index")

    # Extract the upcoming gameweek and the number of free transfers
    upcomingGameweek = fantasyData.loc[fantasyData.index == 0, "upcoming_gameweek"].values[0]
    numFreeTransfers = fantasyData.loc[fantasyData.index == 0, "free_transfers"].values[0]

    # Increment upcoming gameweek
    upcomingGameweek += 1

    # Increment number of free transfers if not reached maximum
    if numFreeTransfers < 2:
        numFreeTransfers += 1

    # Update with the new data
    fantasyData.loc[fantasyData.index == 0, "upcoming_gameweek"] = upcomingGameweek
    fantasyData.loc[fantasyData.index == 0, "free_transfers"] = numFreeTransfers

    # Save fantasy data
    fantasyData.to_csv(util.DATA_PATH + "fantasy/fantasy.csv")


if __name__ == "__main__":
    updateFantasyData()
