import asyncio
import aiohttp

import fantasyData
import playersData
import matchData
import cleanSheetProbability
import playersPredictions
import transfers
import teamSelection

def main():

    print("BOT BEGINNING\n")

    # Update fantasy data
    fantasyData.updateFantasyData()

    # Update players
    playersData.update()

    # Get data from matches from past gameweek
    loop = asyncio.get_event_loop()
    loop.run_until_complete(matchData.updateDataFromPastGameweek())

    # Update fixtures

    # Make clean sheet predictions
    cleanSheetProbability.cleanSheetProbability()

    # Make player predictions
    playersPredictions.makePlayerPredictions()
    playersPredictions.calculateOverallPlayerRating()

    # Transfers
    transfers.makeTransfers()

    # Team selection
    teamSelection.selectStartingLineup()



if __name__ == "__main__":
    main()
