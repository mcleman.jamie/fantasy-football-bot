import pandas as pd
import numpy as np
import json
import requests
import asyncio
import aiohttp
import util

TOTAL_MATCHES = 38.0

def createTeamFiles():

    columns = ["G", "GA", "xG", "xGA", "H/A"]
    rows = np.arange(1, 39)
    priorSeason = ["2020/21"]

    for i in range(1, 21):
        print("Starting " + str(i) + ": " + getNameFromFantasyID(i))
        goals = float(input("GOALS:  ")) / TOTAL_MATCHES
        goalsAgainst = float(input("GOALS AGAINST:  ")) / TOTAL_MATCHES
        xG = float(input("xG:  ")) / TOTAL_MATCHES
        xGA = float(input("xGA:  ")) / TOTAL_MATCHES

        data = pd.DataFrame([], index= np.concatenate((priorSeason, rows), axis=None), columns=columns)

        data.G["2020/21"] = goals
        data.GA["2020/21"] = goalsAgainst
        data.xG["2020/21"] = xG
        data.xGA["2020/21"] = xGA

        data.to_csv(util.DATA_PATH + "teams/" + str(i) + ".csv")
        print(data)
        print("\n")


def calculateAverages():
    columnLabels = ["G", "GA", "xG", "xGA", "H/A"]
    rowLabels = np.arange(1, 39)
    priorSeason = ["2020/21"]
    teams = pd.read_csv(util.DATA_PATH + "teams/teams.csv").set_index("fantasy_id")

    averageG = 0
    averageGA = 0
    averageXG = 0
    averageXGA = 0

    for index, rows in teams.iterrows():
        teamPath = util.DATA_PATH + "teams/" + str(index) + ".csv"
        team = pd.read_csv(teamPath).set_index("week")
        averageG += team.loc["2020/21"]["G"]
        averageGA += team.loc["2020/21"]["GA"]
        averageXG += team.loc["2020/21"]["xG"]
        averageXGA += team.loc["2020/21"]["xGA"]

    averageG /= 20
    averageGA /= 20
    averageXG /= 20
    averageXGA /= 20

    print(averageG)
    print(averageGA)
    print(averageXG)
    print(averageXGA)

    data = pd.DataFrame([], index = np.concatenate((priorSeason, rowLabels), axis=None), columns=columnLabels)
    data.index.name = "week"
    print(data)
    data.G["2020/21"] = averageG
    data.GA["2020/21"] = averageGA
    data.xG["2020/21"] = averageXG
    data.xGA["2020/21"] = averageXGA
    #data = data.set_index("week")
    data.to_csv(util.DATA_PATH + "teams/average.csv")



def getUnderstatIDFromFantasyID(FantasyID):
    a = pd.read_csv(util.DATA_PATH + "teams/teams.csv", index_col="fantasy_id")

    return a.loc[FantasyID]["understat_id"]

def getNameFromFantasyID(FantasyID):
    a = pd.read_csv(util.DATA_PATH + "teams/teams.csv", index_col="fantasy_id")

    return a.loc[FantasyID]["name"]

def getFantasyID(NameToSearchFor):

    a = pd.read_csv(util.DATA_PATH + "teams/teams.csv")
    fantasyID = 0
    i = 0

    for name in a.name:
        if name == NameToSearchFor:
            fantasyID = a.fantasy_id[i]
            break
        i = i+1

    return fantasyID

def getUnderstatID(NameToSearchFor):
    a = pd.read_csv(util.DATA_PATH + "teams/teams.csv")
    understatID = 0
    i = 0

    for name in a.name:
        if name == NameToSearchFor:
            understatID = a.understat_id[i]
            break
        i = i+1

    return understatID

if __name__ == "__main__":
    #print(getUnderstatIDFromFantasyID(5))
    #print(getFantasyID("Leeds"))
    #createTeamFiles()
    calculateAverages()
