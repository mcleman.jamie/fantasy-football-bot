import requests
import pandas as pd
import numpy as np
from understat import Understat
import asyncio
import aiohttp
import teamsData
import util

# TODO:
# - Update data about the players
#   - Update indexes and add any new players
#   - Update the player data after a gameweek
#   - If the season has already started, fill in the passed gameweeks
# - Add price to individual player file
# - Remove unnecessary data (pppm_next_game)

def initialise():

    print("initialise players data")

    # Create players.csv
    createPlayerSummaryFile()

    # Find understat names and understat IDs
    playerSummary = pd.read_csv("../data/players/players.csv")
    for index, row in playerSummary.iterrows():
        loop = asyncio.get_event_loop()
        loop.run_until_complete(findUnderstatIDAndName(row["fantasy_id"]))

    # Manually input missing understat names and understat IDs
    inputMissingUnderstatData()

    # Create individual player files
    loop = asyncio.get_event_loop()
    loop.run_until_complete(createIndividualPlayerFiles(-1))



def update():
    print("update")

    currentPlayers = pd.read_csv(util.DATA_PATH + "players/players.csv")
    url = 'https://fantasy.premierleague.com/api/bootstrap-static/'
    r = requests.get(url)
    apiPlayers = r.json()["elements"]
    playerDataList = []

    columns = ["index", "fantasy_id", "understat_id", "first_name",
    "second_name", "web_name", "understat_name", "team_id", "position", "price",
    "chance_of_playing", "predicted_points_next_game", "predicted_points_next_5",
     "pppm_next_5", "rating_next_5"]

    newPlayersIndexes = []

    # Update indexes
    for playerIndex in range (0, len(apiPlayers)):
        playerID = apiPlayers[playerIndex]["id"]
        chanceOfPlaying = apiPlayers[playerIndex]["chance_of_playing_next_round"]
        if chanceOfPlaying == None:
            chanceOfPlaying = 100

        found = False
        for index, row in currentPlayers.iterrows():
            if playerID == row["fantasy_id"]:
                found = True
                if index != playerIndex:
                    currentPlayers.loc[currentPlayers.index == index, "index"] = playerIndex
                currentPlayers.loc[currentPlayers.index == index, "team_id"] = apiPlayers[playerIndex]["team"]
                currentPlayers.loc[currentPlayers.index == index, "price"] = apiPlayers[playerIndex]["now_cost"] / 10.0
                currentPlayers.loc[currentPlayers.index == index, "chance_of_playing"] = chanceOfPlaying

        # If this player is new
        if found == False:

            # Add this player into the players list
            print("Need to add: " + apiPlayers[playerIndex]["web_name"])
            newPlayersIndexes.append(playerIndex)

            chanceOfPlaying = apiPlayers[playerIndex]["chance_of_playing_next_round"]
            if chanceOfPlaying == None:
                chanceOfPlaying = 100
            playerDataList = [playerIndex, apiPlayers[playerIndex]["id"], None, apiPlayers[playerIndex]["first_name"],
                              apiPlayers[playerIndex]["second_name"], apiPlayers[playerIndex]["web_name"],
                              None, apiPlayers[playerIndex]["team"], apiPlayers[playerIndex]["element_type"],
                              float(apiPlayers[playerIndex]["now_cost"]) / 10.0,
                              chanceOfPlaying, None, None, None, None]



            playerData = pd.DataFrame(np.matrix(playerDataList), index=[0],
                                      columns=columns)

            currentPlayers = currentPlayers.append(playerData, ignore_index=True)


    # Sort by the index and save
    currentPlayers = currentPlayers.set_index("index")
    currentPlayers = currentPlayers.sort_values(by="index", ascending = True)
    currentPlayers.to_csv(util.DATA_PATH + "players/players.csv")


    teams = pd.read_csv(util.DATA_PATH + "teams/teams.csv").set_index("fantasy_id")

    for index, row in currentPlayers.iterrows():
        if np.isnan(row["understat_id"]) == True:
            if apiPlayers[index]["minutes"] > 0:

                name = row["web_name"]
                teamID = row["team_id"]
                teamName = teams.loc[teamID]["name"]

                print("Understat data not found for: " + str(name) + " playing for " + str(teamName))

                understatName = input("Understat name: ")
                if name == "":
                    continue
                understatID = int(input("Understat id:   "))

                currentPlayers.loc[currentPlayers.index == index, "understat_name"] = understatName
                currentPlayers.loc[currentPlayers.index == index, "understat_id"] = understatID

                currentPlayers.to_csv(util.DATA_PATH + "players/players.csv")

    # Create individual player files for new players
    loop = asyncio.get_event_loop()
    for index in newPlayersIndexes:
        id = apiPlayers[index]["id"]
        print("ID: " + str(id))
        loop.run_until_complete(createIndividualPlayerFiles(id))

def createPlayerSummaryFile():

    # Get players from fantasy football api
    url = 'https://fantasy.premierleague.com/api/bootstrap-static/'
    r = requests.get(url)
    players = r.json()["elements"]

    # Define columns
    columns = ["index", "fantasy_id", "understat_id", "first_name",
    "second_name", "web_name", "understat_name", "team_id", "position", "price",
    "chance_of_playing", "predicted_points_next_game", "predicted_points_next_5",
    "pppm_next_5", "rating_next_5"]

    # Create empty data frame
    playersData = pd.DataFrame([], columns=columns)

    # Iterate through every player
    for i in range(0, len(players)):

        chanceOfPlaying = players[i]["chance_of_playing_next_round"]
        if chanceOfPlaying == None:
            chanceOfPlaying = 100

        # Create a list with all the necessary data for the current player
        playerDataList = [i, players[i]["id"], None, players[i]["first_name"],
                          players[i]["second_name"], players[i]["web_name"],
                          None, players[i]["team"], players[i]["element_type"],
                          float(players[i]["now_cost"]) / 10.0,
                          chanceOfPlaying, None, None, None, None]


        playerData = pd.DataFrame(np.matrix(playerDataList), index=[0],
                                  columns=columns)

        playersData = playersData.append(playerData, ignore_index=True)

    playersData = playersData.set_index("index")
    print(playersData)

    playersData.to_csv(util.DATA_PATH + "players/players.csv")


async def findUnderstatIDAndName(FantasyID):

    playerSummary = pd.read_csv(util.DATA_PATH + "players/players.csv")
    name = ""
    success = False

    for index, row in playerSummary.iterrows():
        if row["fantasy_id"] == FantasyID:
            name = row["first_name"] + " " + row["second_name"]
            print(name)
        else:
            continue

        async with aiohttp.ClientSession() as session:

            understat = Understat(session)
            understatData = await understat.get_league_players("epl", 2020, player_name=name)

            if understatData == []:

                name = row["web_name"]
                understatData = await understat.get_league_players("epl", 2020, player_name=name)

                if understatData != []:
                    success = True
                    id = understatData[0]["id"]
            else:
                success = True
                id = understatData[0]["id"]

        if success == True:
            playerSummary.loc[playerSummary.index == index, "understat_name"] = name
            playerSummary.loc[playerSummary.index == index, "understat_id"] = int(id)
            playerSummary = playerSummary.set_index("index")
            playerSummary.to_csv(util.DATA_PATH + "players/players.csv")
            print(str(id) + ": " + str(name) + " successful")

def inputMissingUnderstatData():

    playerSummary = pd.read_csv("../data/players/players.csv")
    playerSummary = playerSummary.set_index("index")

    for index, row in playerSummary.iterrows():
        name = row["web_name"]
        if np.isnan(row["understat_id"]) == True:

            if row["minutes"] != 0:

                print("Data not found for: " + str(name))
                name = input("Understat name: ")
                if name == "":
                    continue
                id = int(input("Understat id:   "))

                playerSummary.loc[playerSummary.index == index, "understat_name"] = name
                playerSummary.loc[playerSummary.index == index, "understat_id"] = id

                playerSummary.to_csv(util.DATA_PATH + "players/players.csv")

async def createIndividualPlayerFiles(FantasyID):

    playerSummary = pd.read_csv(util.DATA_PATH + "players/players.csv")
    fantasyData = pd.read_csv(util.DATA_PATH + "fantasy/fantasy.csv")
    upcomingGameweek = int(fantasyData.loc[fantasyData.index == 0, "upcoming_gameweek"].values[0])

    url = 'https://fantasy.premierleague.com/api/bootstrap-static/'
    r = requests.get(url)
    players = r.json()["elements"]

    rows = np.arange(1, 39)
    rows = np.concatenate((["2020/21"], rows, ["total"]))
    columns = []

    for index, row in playerSummary.iterrows():

        id = row["fantasy_id"]

        if FantasyID > -1:
            if id != FantasyID:
                continue

        path = util.DATA_PATH + "players/players/" + str(id) + ".csv"
        position = row["position"]
        name = row["understat_name"]
        try:
            if np.isnan(name) == True:
                name = ""
        except:
            pass

        player = players[row["index"]]

        if position == 1:
            columns = ["minutes", "saves", "bonus", "points", "price", "available"]
        else:
            columns = ["minutes", "G", "A", "xG", "xA", "bonus", "points", "price", "available"]

        data = pd.DataFrame([], index=rows, columns=columns)
        data.index.name = "gameweek"

        # If the season hasn't started:
        if upcomingGameweek == 1:

            data.minutes["2020/21"] = player["minutes"]
            data.bonus["2020/21"] = float(player["bonus"])
            data.points["2020/21"] = float(player["total_points"])
            data.price["2020/21"] = None
            data.available["2020/21"] = 0

            if position == 1:
                data.saves["2020/21"] = float(player["saves"])
                data.loc["total"] = [0, 0, 0, 0, 0, 0]
            else:
                data.G["2020/21"] = float(player["goals_scored"])
                data.A["2020/21"] = float(player["assists"])
                data.loc["total"] = [0, 0, 0, 0, 0, 0, 0, 0, 0]

                if name != "":
                    async with aiohttp.ClientSession() as session:
                        understat = Understat(session)
                        understatData = await understat.get_league_players("epl", 2020, player_name=name)

                        if (understatData == []):
                            print("Understat data not found with name: " + str(name))
                            data.xG["2020/21"] = 0
                            data.xA["2020/21"] = 0
                        else:
                            data.xG["2020/21"] = float(understatData[0]["xG"])
                            data.xA["2020/21"] = float(understatData[0]["xA"])
                else:
                    data.xG["2020/21"] = 0
                    data.xA["2020/21"] = 0

        # The season has already started
        else:

            if position == 1:
                data.loc["2020/21"] = [0, 0, 0, 0, 0, 0]
                data.loc["total"] = [0, 0, 0, 0, 0, 0]
                for i in range(1, upcomingGameweek):
                    data.loc[str(i)] = [0, 0, 0, 0, 0, 0]
            else:
                data.loc["2020/21"] = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                data.loc["total"] = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                for i in range(1, upcomingGameweek):
                    data.loc[str(i)] = [0, 0, 0, 0, 0, 0, 0, 0, 0]

        data.to_csv(path)

def playerLikelihoodToPlayGameweek1():

    players = pd.read_csv(util.DATA_PATH + "players/players.csv").set_index("index")

    likelihoodClass = 1
    likelihood = 0
    likelihoodData = pd.DataFrame([], columns=["fantasy_id", "likelihood_to_play"])

    for index, row in players.iterrows():

        fantasy_id = row["fantasy_id"]
        team_id = row["team_id"]
        teamName = teamsData.getNameFromFantasyID(team_id)
        print(row["web_name"] + " : " + teamName)

        success = False
        while (success == False):
            likelihoodClass = input("Enter class (1-4):  ")
            success = True
            if likelihoodClass == "1":
                likelihood = 0.9
            elif likelihoodClass == "2":
                likelihood = 0.6
            elif likelihoodClass == "3":
                likelihood = 0.3
            elif likelihoodClass == "4":
                likelihood = 0
            else:
                success = False

        singlePlayerLikelihoodData = pd.DataFrame(np.matrix([fantasy_id, likelihood]), index=[0], columns=["fantasy_id", "likelihood_to_play"])
        likelihoodData = likelihoodData.append(singlePlayerLikelihoodData, ignore_index=True)

    likelihoodData = likelihoodData.set_index("fantasy_id")
    likelihoodData.to_csv(util.DATA_PATH + "players/likelihoodToPlay.csv")


if __name__ == "__main__":
    #initialise()
    update()
