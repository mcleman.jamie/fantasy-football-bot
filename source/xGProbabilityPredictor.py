import pandas as pd
import numpy as np
import util

# Lists of all the xGCs for clean sheets and non clean sheets
cleanSheetXGCList = []
nonCleanSheetXGCList = []

# Open the scores and xG data for the last 3 seasons
xGScoresData = pd.read_csv(util.DATA_PATH + "2018-21scoresAndXG.csv")
# Iterate through each game
for index, row in xGScoresData.iterrows():

    # Home team kept a clean sheet
    if row["score"][2] == "0":
        cleanSheetXGCList.append(float(row["away_xG"]))
    # Home team didn't keep a clean sheet
    else:
        nonCleanSheetXGCList.append(float(row["away_xG"]))

    # Away team kept a clean sheet
    if row["score"][0] == "0":
        cleanSheetXGCList.append(float(row["home_xG"]))
    # Away team didn't keep a clean sheet
    else:
        nonCleanSheetXGCList.append(float(row["home_xG"]))

# Define intervals to sample the data with
# Between 0 and 3 with intervals of 0.1
intervals = np.arange(0, 3.0, 0.1)

# Create histograms of the clean sheet and non clean sheet data
CSHistogram = pd.cut(cleanSheetXGCList, intervals).value_counts()
nonCSHistogram = pd.cut(nonCleanSheetXGCList, intervals).value_counts()

# Create a probability list containing 1 for the clean sheet probability when
# the xGC is 0
CSProbabilities = [1]

# Iterate through every interval and calculate the percentage clean sheets
for i in range(0, intervals.size - 1):
    CSProbability = CSHistogram[i] / (CSHistogram[i] + nonCSHistogram[i])
    CSProbabilities.append(CSProbability)

# Combine the cs probability and interval into a dataframe
probabilityData = pd.DataFrame(np.vstack((intervals, CSProbabilities)).T,
                               columns=["xG_value", "probability"])
print(probabilityData)

# Calculate coefficients based on this data. Quadratic
coefficients = np.polyfit(probabilityData["xG_value"],
                          probabilityData["probability"], 2)

# Put the coefficients into a dataframe and save to a csv file
coefficientData = pd.DataFrame(coefficients, columns=["coefficient"])
coefficientData.to_csv(util.DATA_PATH + "xGACleanSheetProbabilityFormula.csv")
