import pandas as pd
import numpy as np
import util

def makePlayerPredictions():

    # Load in all the necessary data
    players = pd.read_csv(util.DATA_PATH + "players/players.csv").set_index("index")
    teams = pd.read_csv(util.DATA_PATH + "teams/teams.csv").set_index("fantasy_id")
    teamAverages = pd.read_csv(util.DATA_PATH + "teams/average.csv").set_index("week")
    cleanSheetData = pd.read_csv(util.DATA_PATH + "teams/cleanSheetsPredicted.csv").set_index("fantasy_id")
    fantasyData = pd.read_csv(util.DATA_PATH + "fantasy/fantasy.csv")

    upcomingGameweek = int(fantasyData.loc[fantasyData.index == 0, "upcoming_gameweek"].values[0])
    averageXGA = teamAverages.loc["2020/21"]["xGA"]

    # Iterate through every team
    for team in range(1, 21):

        # Get their next 5 fixtures
        next5Fixtures = getNextFiveFixtures(team, upcomingGameweek)
        next5FixturesXGAPerformanceRatio = []

        # For each of this teams next 5 games
        for fixture in next5Fixtures:

            # Get the data on the opposition
            path = util.DATA_PATH + "teams/" + str(fixture[0]) + ".csv"
            oppositionData = pd.read_csv(path).set_index("week")

            # Calculate the ratio between the oppositions defensive performance compared to the average
            ratio = oppositionData.loc["2020/21"]["xGA"] / averageXGA
            next5FixturesXGAPerformanceRatio.append(ratio)

        # Get the clean sheet likelihoods for each of the next 5 games
        cleanSheetPercentages = []
        for i in range(0, 5):
            cleanSheetPercentages.append(cleanSheetData.loc[team][str(upcomingGameweek + i)])

        # Iterate through every player
        for index, row in players.iterrows():

            # We only want players for the current team we are working on
            if row["team_id"] == team:

                # Load in the player data
                id = row["fantasy_id"]
                path = util.DATA_PATH + "players/players/" + str(id) + ".csv"
                playerData = pd.read_csv(path).set_index("gameweek")
                print(row["web_name"])

                # Initialise variables
                pointsEstimateNextGame = 0
                pointsEstimateNext5 = 0
                previousSeasonMinutes = playerData.loc["2020/21"]["minutes"]
                previousSeasonNum90s = previousSeasonMinutes / 90.0
                currentSeasonMinutes = playerData.loc["total"]["minutes"]
                currentSeasonNum90s = currentSeasonMinutes / 90.0
                chanceOfPlaying = players.loc[players.fantasy_id == id, "chance_of_playing"].values[0] / 100.0

                # Calculate the ratio of current season data and previous season data
                # The more the player has played in the current season the more heavily that data is relied on
                if previousSeasonMinutes + currentSeasonMinutes == 0:
                    currentSeasonRatio = 0
                    previousSeasonRatio = 0
                elif previousSeasonMinutes < 200:
                    currentSeasonRatio = min(currentSeasonMinutes / 400.0, 1.0)
                    previousSeasonRatio = 1.0 - currentSeasonRatio
                else:
                    currentSeasonRatio = min(currentSeasonMinutes / 800.0, 1.0)
                    previousSeasonRatio = 1.0 - currentSeasonRatio

                # Start to make points predictions

                # Goalkeepers
                if row["position"] == 1:

                    # Get saves per 90 and bonus per 90
                    savesPer90 = 0
                    bonusPer90 = 0
                    if previousSeasonNum90s > 0 and previousSeasonRatio > 0.0:
                        savesPer90 += (playerData.loc["2020/21"]["saves"] / previousSeasonNum90s) * previousSeasonRatio
                        bonusPer90 += (playerData.loc["2020/21"]["bonus"] / previousSeasonNum90s) * previousSeasonRatio

                    if currentSeasonNum90s > 0:
                        savesPer90 += (playerData.loc["total"]["saves"] / currentSeasonNum90s) * currentSeasonRatio
                        bonusPer90 += (playerData.loc["total"]["bonus"] / currentSeasonNum90s) * currentSeasonRatio

                    #  Estimate points in the next game
                    pointsEstimateNextGame += 2
                    pointsEstimateNextGame += cleanSheetPercentages[0] * 4
                    pointsEstimateNextGame += savesPer90 / 3.0
                    pointsEstimateNextGame += bonusPer90

                    # Estimate points over the next 5 games
                    pointsEstimateNext5 += 2 * 5.0
                    pointsEstimateNext5 += (savesPer90 / 3.0) * 5.0
                    pointsEstimateNext5 += bonusPer90 * 5.0
                    for i in range(0, 5):
                        pointsEstimateNext5 += cleanSheetPercentages[i] * 4

                # Outfield players
                else:

                    # Get xG per 90, xA per 90 and bonus per 90
                    xGPer90 = 0
                    xAPer90 = 0
                    bonusPer90 = 0
                    if previousSeasonNum90s > 0 and previousSeasonRatio > 0.0:
                        xGPer90 += (playerData.loc["2020/21"]["xG"] / previousSeasonNum90s) * previousSeasonRatio
                        xAPer90 += (playerData.loc["2020/21"]["xA"] / previousSeasonNum90s) * previousSeasonRatio
                        bonusPer90 += (playerData.loc["2020/21"]["bonus"] / previousSeasonNum90s) * previousSeasonRatio

                    if currentSeasonNum90s > 0:
                        xGPer90 += (playerData.loc["total"]["xG"] / currentSeasonNum90s) * currentSeasonRatio
                        xAPer90 += (playerData.loc["total"]["xA"] / currentSeasonNum90s) * currentSeasonRatio
                        bonusPer90 += (playerData.loc["total"]["bonus"] / currentSeasonNum90s) * currentSeasonRatio

                    # Estimate points over the next 5 games
                    for i in range(0, 5):

                        pointsEstimateNext5 += 2
                        pointsEstimateNext5 += xAPer90 * next5FixturesXGAPerformanceRatio[i] * 3

                        goalsEstimate = xGPer90 * next5FixturesXGAPerformanceRatio[i]

                        # Different amount of points per goal depending on position
                        if row["position"] == 2:
                            pointsEstimateNext5 += goalsEstimate * 6
                        elif row["position"] == 3:
                            pointsEstimateNext5 += goalsEstimate * 5
                        elif row["position"] == 4:
                            pointsEstimateNext5 += goalsEstimate * 4

                        pointsEstimateNext5 += bonusPer90

                        # If this is the first game
                        if i == 0:
                            pointsEstimateNextGame = pointsEstimateNext5


                # Calculate predicted minutes
                matchesAvailable = len(playerData.loc[playerData["available"] == 1].index)
                if matchesAvailable == 0:
                    predictedMinutes = 0
                    predictedMinutesPercentage = 0
                else:
                    predictedMinutes = currentSeasonMinutes / matchesAvailable
                    predictedMinutes *= chanceOfPlaying
                    if predictedMinutes == 0:
                        predictedMinutesPercentage = 0
                    else:
                        predictedMinutesPercentage = predictedMinutes / 90.0

                # Scale the points estimate by the predicted minutes
                pointsEstimateNextGame = pointsEstimateNextGame * predictedMinutesPercentage
                pointsEstimateNext5 = pointsEstimateNext5 * predictedMinutesPercentage

                # Calculate the points per million over the next 5 games
                pppmNext5 = pointsEstimateNext5 / row["price"]

                # Store the new data
                players.loc[players.index == index, "predicted_points_next_game"] = pointsEstimateNextGame
                players.loc[players.index == index, "predicted_points_next_5"] = pointsEstimateNext5
                players.loc[players.index == index, "pppm_next_5"] = pppmNext5

    # Write the new data to file
    players.to_csv(util.DATA_PATH + "players/players.csv")


def calculateOverallPlayerRating():

    players = pd.read_csv(util.DATA_PATH + "players/players.csv").set_index("index")

    for position in range(1, 5):
        print("\nPOSITION: " + str(position) + "\n")

        playersByPosition = players[players["position"] == position]
        playersByPosition = playersByPosition.sort_values(by="pppm_next_5", ascending = False)
        print(playersByPosition)
        playersByPosition = playersByPosition.reset_index(drop=True)
        #print(playersByPosition)

        highestPPPM = playersByPosition.loc[0]["pppm_next_5"]
        print("Highest PPPM: " + str(highestPPPM))
        playersByPosition = playersByPosition.sort_values(by="predicted_points_next_5", ascending = False)
        print(playersByPosition)
        playersByPosition = playersByPosition.reset_index(drop=True)
        highestPredictedPoints = playersByPosition.loc[0]["predicted_points_next_5"]
        print("Highest predicted points: " + str(highestPredictedPoints))

        for index, row in playersByPosition.iterrows():
            players.loc[players.fantasy_id == row["fantasy_id"], "rating_next_5"] = (row["pppm_next_5"] / highestPPPM) * (row["predicted_points_next_5"] / highestPredictedPoints)

    #print(players)
    players.to_csv(util.DATA_PATH + "players/players.csv")

    for position in range(1, 5):
        print("\nPOSITION: " + str(position) + "\n")
        playersByPosition = players[players["position"] == position]
        playersByPosition = playersByPosition.sort_values(by="predicted_points_next_5", ascending = False)
        playersByPosition = playersByPosition.reset_index(drop=True)
        for i in range(0, 10):
            print(playersByPosition.loc[i]["web_name"])


# Obtain a teams next 5 fixtures
# Need to change so that it gets the fixtures over the next 5 weeks
# And deal with BGWs and DGWs
def getNextFiveFixtures(TeamID, FirstGameweek):

    # Load in fixtures data
    fixtures = pd.read_csv(util.DATA_PATH + "fixtures.csv")

    # Initialise variables
    next5 = [None, None, None, None, None]
    currentFixture = [None, None]
    upcomingGameweek = FirstGameweek
    foundFixture = False

    # Iterate through all fixtures
    for index, row in fixtures.iterrows():
        # If less than the gameweek we're looking for
        if row["week"] < upcomingGameweek:
            continue

        # If we're at the correct gameweek
        elif row["week"] == upcomingGameweek:

            # Found a fixture containing the teamID
            if row["home"] == TeamID:
                currentFixture[0] = row["away"]
                currentFixture[1] = "H"
                foundFixture = True

            elif row["away"] == TeamID:
                currentFixture[0] = row["home"]
                currentFixture[1] = "A"
                foundFixture = True

            if foundFixture == True:
                # Save the current fixture to data
                next5[upcomingGameweek - FirstGameweek] = currentFixture
                # If weve found 5, return the data
                if upcomingGameweek == FirstGameweek + 4:
                    return next5
                # Otherwise go the next week
                else:
                    upcomingGameweek += 1
                    currentFixture = [None, None]
                    foundFixture = False

# DEPRECIATED --------------------
def getLikelihoodToPlay(FantasyID):
    likelihoodToPlay = pd.read_csv(util.DATA_PATH + "players/likelihoodToPlay.csv")

    for index, row in likelihoodToPlay.iterrows():
        if row["fantasy_id"] == FantasyID:
            return row["likelihood_to_play"]
# --------------------------------

if __name__ == "__main__":
    makePlayerPredictions()
    calculateOverallPlayerRating()
