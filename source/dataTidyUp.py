import util
import pandas as pd
import numpy as np

# DO NOT RUN AGAIN
def tidyTeamData():
    defaultTeamPath = util.DATA_PATH + "teams/"

    for teamID in range(1, 21):
        teamData = pd.read_csv(defaultTeamPath + str(teamID) + ".csv")
        teamData.index.name = "index"
        print(teamData)

        teamData.to_csv(defaultTeamPath + str(teamID) + ".csv")

# DO NOT RUN AGAIN
def tidyPlayerData():

    defaultPlayerPath = util.DATA_PATH + "players/players/"
    playerSummary = pd.read_csv(util.DATA_PATH + "players/players.csv")

    for index, row in playerSummary.iterrows():
        fantasyID = row["fantasy_id"]
        playerPath = defaultPlayerPath + str(fantasyID) + ".csv"
        playerData = pd.read_csv(playerPath).set_index("gameweek")
        for index, row in playerData.iterrows():
            try:
                indexInt = int(index)
                if indexInt >= 18:
                    print(indexInt)
                    playerData.drop(index, inplace=True)
                else:
                    if np.isnan(row["minutes"]) == True:
                        row["available"] = 0
            except:
                pass

        playerData.to_csv(playerPath)

    for index, row in playerSummary.iterrows():
        fantasyID = row["fantasy_id"]
        playerPath = defaultPlayerPath + str(fantasyID) + ".csv"
        playerData = pd.read_csv(playerPath)
        playerData.index.name = "index"
        playerData.to_csv(playerPath)


if __name__ == "__main__":
    print("THIS CODE SHOULD NOT BE RUN AGAIN!")
