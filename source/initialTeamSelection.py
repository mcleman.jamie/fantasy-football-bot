import pandas as pd
import numpy as np
import util

BUDGETS = {1: [8.5, 12], 2: [22.5, 33.0], 3: [31.0, 50.0], 4: [19.0, 33.0]}
POSITION_NUMBER = {1: 2, 2: 5, 3: 5, 4: 3}
POSITION_COLUMNS = {1:["price", "projected_points", "pppm", "1", "2"],
                    2:["price", "projected_points", "pppm", "1", "2", "3", "4", "5"],
                    3:["price", "projected_points", "pppm", "1", "2", "3", "4", "5"],
                    4:["price", "projected_points", "pppm", "1", "2", "3"]}



GOALKEEPERS_BUDGET = 9.5
DEFENDERS_BUDGET = 28
MIDFIELDERS_BUDGET = 40.0
FORWARDS_BUDGET = 22.5

# GOALKEEPERS: 8.5 -> 12
# DEFENDERS: 22.5 -> 33
# MIDFIELDERS: 31 -> 50
# FORWARDS: 19 -> 33

# CHECK IF BREAKING THE PLAYERS FROM SAME TEAM CONSTRAINT:
#    - REMOVE AN OFFENDING PLAYER AND IGNORE PLAYERS FROM THAT TEAM
#    - WHICHEVER PLAYER REDUCES THE TOTAL POINTS THE LEAST IS REPLACED
# IF THERE IS BUDGET REMAINING:
#    - ADD THE SURPLUS ONTO EACH POSITION AND FIND WHICH HAS THE BEST EFFECT
# SAVE THE RESULTS INTO A CSV FILE

def newInitialTeamSelection():
    players = pd.read_csv(util.DATA_PATH + "players/players.csv").set_index("index")
    optimumPointsByPosition = {}

    for position in range(1, 5):
        minBudget = BUDGETS[position][0]
        maxBudget = BUDGETS[position][1]
        optimumPointsByPosition[position] = pd.DataFrame([], columns=POSITION_COLUMNS[position])

        playersByPosition = players[players["position"] == position]
        playerIDs = []

        for budget in np.arange(minBudget, maxBudget + 0.5, 0.5):
            #print("TOTAL BUDGET: " + str(budget))

            numPlayersNeeded = POSITION_NUMBER[position]
            budgetRemaining = budget
            projectedPoints = 0
            playerIDs = []

            while numPlayersNeeded > 0:
                playerBudget = roundDownDecimal(budgetRemaining / numPlayersNeeded)
                #print("Player budget: " + str(playerBudget))
                eligiblePlayers = playersByPosition[playersByPosition["price"] <= playerBudget].sort_values(by="predicted_points_next_5", ascending=False).reset_index()

                index = 0
                fantasyID = eligiblePlayers[eligiblePlayers.index == index]["fantasy_id"].values[0]
                while fantasyID in playerIDs:
                    index += 1
                    fantasyID = eligiblePlayers[eligiblePlayers.index == index]["fantasy_id"].values[0]

                playerIDs.append(fantasyID)
                #print(eligiblePlayers[eligiblePlayers.index == index]["web_name"].values[0])
                budgetRemaining -= eligiblePlayers[eligiblePlayers.index == index]["price"].values[0]
                projectedPoints += eligiblePlayers[eligiblePlayers.index == index]["predicted_points_next_5"].values[0]
                numPlayersNeeded -= 1

            price = budget - budgetRemaining
            if len(optimumPointsByPosition[position][optimumPointsByPosition[position]["price"] == price].values) == 0:
                if len(optimumPointsByPosition[position][optimumPointsByPosition[position]["projected_points"] > projectedPoints].values) == 0:
                    data = []
                    data.append(price)
                    data.append(projectedPoints)
                    data.append(projectedPoints / price)
                    for id in playerIDs:
                        data.append(id)
                    optimumPointsByPosition[position] = optimumPointsByPosition[position].append(pd.DataFrame(np.matrix(data), index=[0], columns=POSITION_COLUMNS[position]), ignore_index=True)

        print(optimumPointsByPosition[position])

    lineupIDs = {1: 0, 2: 0, 3: 0, 4: 0}
    currentTeamPrice = 0

    for position in range(1, 5):
        sortedData = optimumPointsByPosition[position].sort_values(by="pppm", ascending=False).reset_index()
        currentTeamPrice += sortedData[sortedData.index == lineupIDs[position]]["price"].values[0]
        lineupIDs[position] = sortedData[sortedData.index == lineupIDs[position]]["index"].values[0]
        print(sortedData)

    print("Team price: " + str(currentTeamPrice))
    print(lineupIDs)

    improvementRatios = {}
    for position in range(1, 5):
        #optimumPointsByPosition[position] = optimumPointsByPosition[position].sort_values(by="price", ascending=False).reset_index()
        optimumPointsByPosition[position].index.name = "index"
        print(optimumPointsByPosition[position])

    foundOptimum = False
    while foundOptimum == False:

        currentTeamPrice = 0
        for position in range(1, 5):
            currentTeamPrice += optimumPointsByPosition[position][optimumPointsByPosition[position].index == lineupIDs[position]]["price"].values[0]
        print("\n")
        print("TEAM PRICE: " + str(currentTeamPrice))
        print(lineupIDs)

        for position in range(1, 5):

            currentPrice = optimumPointsByPosition[position][optimumPointsByPosition[position].index == lineupIDs[position]]["price"].values[0]
            nextPrice = optimumPointsByPosition[position][optimumPointsByPosition[position].index == lineupIDs[position] + 1]["price"]
            if len(nextPrice.values) == 0:
                improvementRatios[position] = -1
                continue
            else:
                nextPrice = nextPrice.values[0]
            priceDifference = nextPrice - currentPrice
            print("pos/priceDifference: " + str(position) + "/" +str(priceDifference))
            if currentTeamPrice + priceDifference > 100.0:
                improvementRatios[position] = -1
                continue

            currentPoints = optimumPointsByPosition[position][optimumPointsByPosition[position].index == lineupIDs[position]]["projected_points"].values[0]
            nextPoints = optimumPointsByPosition[position][optimumPointsByPosition[position].index == lineupIDs[position] + 1]["projected_points"].values[0]
            pointsDifference = nextPoints - currentPoints

            ratio = pointsDifference / priceDifference
            #print("Price difference: " + str(priceDifference))
            #print("Points difference: " + str(pointsDifference))
            #print("Ratio: " + str(ratio))

            improvementRatios[position] = ratio


        for position in range(1, 5):
            if improvementRatios[position] == -1:
                if position == 4:
                    foundOptimum = True
            else:
                break

        if foundOptimum == True:
            print(lineupIDs)
            team = []
            for position in range(1, 5):
                lineupID = lineupIDs[position]
                for i in range(1, 6):
                    try:
                        playerID = optimumPointsByPosition[position][optimumPointsByPosition[position].index == lineupID][str(i)]
                        if len(playerID.values) > 0:
                            team.append(playerID.values[0])
                    except:
                        break
            print(team)
            return team

        biggestRatio = 0
        biggestRatioPosition = 0
        for position in range(1, 5):
            if improvementRatios[position] > biggestRatio:
                biggestRatio = improvementRatios[position]
                biggestRatioPosition = position

        lineupIDs[biggestRatioPosition] += 1
        print("Improving: " + str(biggestRatioPosition))


def roundDownDecimal(Num):
    return np.floor(Num * 2.0) / 2.0

def selectPlayersInPosition(Position, NumberToSelect, NumberToSearch, Budget):
    players = pd.read_csv(util.DATA_PATH + "players/players.csv")
    playersByPosition = players[players["position"] == Position]

    playersByPosition = playersByPosition.sort_values(by="rating_next_5", ascending = False)
    playersByPosition = playersByPosition.reset_index(drop=True)
    playersByPosition.index.name = "rank"

    optimumPermutation = -1
    optimumPoints = 0
    currentPoints = 0
    currentPrice = 0

    permutations = getPermutations(NumberToSearch, NumberToSelect)

    for i in range(0, len(permutations)):
        currentPoints = 0
        currentPrice = 0
        for j in range(0, len(permutations[i])):
            currentPoints += playersByPosition.loc[permutations[i][j]]["predicted_points_next_5"]
            currentPrice += playersByPosition.loc[permutations[i][j]]["price"]
        if currentPrice <= Budget:
            if currentPoints > optimumPoints:
                optimumPoints = currentPoints
                optimumPermutation = i

    optimumPlayersIDs = []


    if optimumPermutation >= 0:
        for i in range(0, len(permutations[optimumPermutation])):
            optimumPlayersIDs.append(playersByPosition.loc[permutations[optimumPermutation][i]]["fantasy_id"])

    return [optimumPlayersIDs, optimumPoints]

def getPermutations(Total, NumPerPermutation):

    permutations = []
    permutation = []
    for i in range(0, NumPerPermutation):
        permutation.append(i)
    permutations.append(list(permutation))

    currentIndex = 1
    while True:
        if permutation[NumPerPermutation - currentIndex] + 1 < Total - (currentIndex - 1):
            permutation[NumPerPermutation - currentIndex] += 1
            while currentIndex > 1:
                currentIndex -= 1
                permutation[NumPerPermutation - currentIndex] = permutation[NumPerPermutation - currentIndex - 1] + 1
            permutations.append(list(permutation))
        else:
            if currentIndex == NumPerPermutation:
                break
            currentIndex += 1

    return permutations

def saveTeam(Team):
    playersData = pd.read_csv(util.DATA_PATH + "players/players.csv").set_index("index")

    columns = ["fantasy_id", "purchase_price"]

    teamData = pd.DataFrame([], columns=columns)

    player = []
    for playerID in Team:

        price = playersData.loc[playersData["fantasy_id"] == playerID, "price"].values[0]
        player = [playerID, price]

        playerData = pd.DataFrame(np.matrix(player), index=[0], columns=columns)
        teamData = teamData.append(playerData, ignore_index = True)

    teamData = teamData.set_index("fantasy_id")

    teamData.to_csv(util.DATA_PATH + "fantasy/team.csv")

def initialiseFantasyData(MoneyITB):

    columns = ["upcoming_gameweek", "money_itb", "free_transfers"]

    fantasyData = pd.DataFrame(np.matrix([1, MoneyITB, 1]), columns=columns)

    fantasyData.to_csv(util.DATA_PATH + "fantasy/fantasy.csv")

def checkSameTeamConstrain(Team):

    playersData = pd.read_csv(util.DATA_PATH + "players/players.csv").set_index("index")

    teamFrequencies = {}
    for i in range(1, 21):
        teamFrequencies[i] = 0

    for playerID in Team:
        currentTeamID = playersData.loc[playersData["fantasy_id"] == playerID, "team_id"].values[0]
        teamFrequencies[currentTeamID] += 1

    for item in teamFrequencies:
        if (teamFrequencies[item]) > 3:
            return item

    return -1


if __name__ == "__main__":

    team = newInitialTeamSelection()


    #goalkeepers = selectPlayersInPosition(1, 2, 30, GOALKEEPERS_BUDGET)
    #defenders = selectPlayersInPosition(2, 5, 20, DEFENDERS_BUDGET)
    #midfielders = selectPlayersInPosition(3, 5, 20, MIDFIELDERS_BUDGET)
    #forwards = selectPlayersInPosition(4, 3, 40, FORWARDS_BUDGET)

    #team = goalkeepers[0] + defenders[0] + midfielders[0] + forwards[0]

    if checkSameTeamConstrain(team) == -1:

        saveTeam(team)

        playersData = pd.read_csv(util.DATA_PATH + "players/players.csv").set_index("index")

        teamCost = 0

        print("TEAM: \n")
        for playerID in team:
            currentTeamID = playersData.loc[playersData["fantasy_id"] == playerID, "team_id"].values[0]
            print(playersData.loc[playersData["fantasy_id"] == playerID, "web_name"].values[0])
            teamCost += playersData.loc[playersData["fantasy_id"] == playerID, "price"].values[0]

        print("\nTeam cost:")
        print(teamCost)

        initialiseFantasyData(100 - teamCost)
